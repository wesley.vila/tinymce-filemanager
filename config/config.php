<?php
define('URL_BASE', 'http://localhost/modelos/tiny-filemanager/');
/**
 * TODOS REFERENTES A PASTAS DENTRO DO ASSETS
 * UPLOADDIR -> CAMINHO ABSOLUTO DA PASTA IMAGENS
 * 
 */
define('FILEMANAGER', [
    'UPLOAD_DIR' => '/modelos/tiny-filemanager/assets/img/images/',
    'CURRENT_PATH' => '../../../../img/images/', 
    'THUMBS_BASE_PATH' => '../../../../img/thumbs/',
    'THUMBS_UPLOAD_DIR' => '../../../../img/thumbs/'
]);

define('URL_TEMPLATES_TYNE', URL_BASE . 'assets/js/libs/tinymce/templates/');